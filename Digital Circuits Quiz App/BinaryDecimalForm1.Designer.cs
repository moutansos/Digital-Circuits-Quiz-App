﻿namespace Digital_Circuits_Quiz_App
{
    partial class BinaryDecimalForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SubmitButton1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SubmitButton1
            // 
            this.SubmitButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SubmitButton1.Location = new System.Drawing.Point(12, 214);
            this.SubmitButton1.Name = "SubmitButton1";
            this.SubmitButton1.Size = new System.Drawing.Size(569, 73);
            this.SubmitButton1.TabIndex = 0;
            this.SubmitButton1.Text = "Check";
            this.SubmitButton1.UseVisualStyleBackColor = true;
            this.SubmitButton1.Click += new System.EventHandler(this.SubmitButton1_Click);
            // 
            // BinaryDecimalForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.ClientSize = new System.Drawing.Size(593, 299);
            this.Controls.Add(this.SubmitButton1);
            this.ForeColor = System.Drawing.Color.Cyan;
            this.Name = "BinaryDecimalForm1";
            this.Text = "Digital Circuits Quiz App - Binary and Decimal";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button SubmitButton1;
    }
}