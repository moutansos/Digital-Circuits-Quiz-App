﻿namespace Digital_Circuits_Quiz_App
{
    partial class QuizSelectForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartQuizButton1 = new System.Windows.Forms.Button();
            this.DecimalAndBinaryRadioButton1 = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // StartQuizButton1
            // 
            this.StartQuizButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StartQuizButton1.Location = new System.Drawing.Point(13, 339);
            this.StartQuizButton1.Name = "StartQuizButton1";
            this.StartQuizButton1.Size = new System.Drawing.Size(740, 99);
            this.StartQuizButton1.TabIndex = 0;
            this.StartQuizButton1.Text = "Start Quiz";
            this.StartQuizButton1.UseVisualStyleBackColor = true;
            this.StartQuizButton1.Click += new System.EventHandler(this.StartQuizButton1_Click);
            // 
            // DecimalAndBinaryRadioButton1
            // 
            this.DecimalAndBinaryRadioButton1.AutoSize = true;
            this.DecimalAndBinaryRadioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DecimalAndBinaryRadioButton1.Location = new System.Drawing.Point(13, 13);
            this.DecimalAndBinaryRadioButton1.Name = "DecimalAndBinaryRadioButton1";
            this.DecimalAndBinaryRadioButton1.Size = new System.Drawing.Size(115, 17);
            this.DecimalAndBinaryRadioButton1.TabIndex = 1;
            this.DecimalAndBinaryRadioButton1.TabStop = true;
            this.DecimalAndBinaryRadioButton1.Text = "Decimal and Binary";
            this.DecimalAndBinaryRadioButton1.UseVisualStyleBackColor = true;
            // 
            // QuizSelectForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.ClientSize = new System.Drawing.Size(765, 450);
            this.Controls.Add(this.DecimalAndBinaryRadioButton1);
            this.Controls.Add(this.StartQuizButton1);
            this.ForeColor = System.Drawing.Color.Cyan;
            this.Name = "QuizSelectForm1";
            this.Text = "Digital Circuits Quiz App";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button StartQuizButton1;
        private System.Windows.Forms.RadioButton DecimalAndBinaryRadioButton1;
    }
}

